#!/usr/bin/env python
# -*- encoding: utf-8 -*-

##Imports
import concurrent.futures
import csv
from datetime import datetime
import dill
import io
import json
import logging
import os
import queue
import socket
import threading
import struct
import sys
import time
import uuid

def generateUUID():
    return uuid.uuid4()

class Service:
    '''Generic response demonstrating a service.'''
    def __init__(self,payload=f"The server has received your request for service.",*args,**kwargs):
        return payload

class Messenger:
    '''
    Daemon to receive a request, execute a service, and queue response.
    Service must be a callable that returns a response.
    '''
    def __init__(self, socket=None, address=None, io_q=None, connection_q=None, registration_q=None, disk_q=None, service=Service, uuid_func=generateUUID):
        self.socket = socket
        self.address = address
        self.io_q = io_q
        self.connection_q = connection_q
        self.registration_q = registration_q
        self.disk_q = disk_q
        self.service = service
        self.uuid_func = uuid_func
        
        self._receive_buffer = b""
        self._send_buffer = b""
        self.payload = b""
        
        self._jsonheader_len = None
        self._jsonheader = None
        self._exit = False
        
        ##Add to the open connection pool an entry in the table in form of: (uid, status, socket, address)
        self.connection_q.put((5,"online", self.socket, self.address))

    def _buildMessage(self, content_type="text/pickle", content_encoding="utf-8", destination="Echo client", active=True, uuid_to=False,uuid_from=False, content_bytes="Request from client."):
        content_bytes = dill.dumps(content_bytes)
        jsonheader = {
            "byteorder": sys.byteorder,
            "content-type": content_type,
            "content-encoding": content_encoding,
            "content-length": len(content_bytes),
            "destination": destination,
            "active": active,
            "uuid_to": uuid_to,
            "uuid_from": uuid_from,
        }
        jsonheader_bytes = self._jsonEncode(jsonheader, "utf-8")
        message_hdr = struct.pack("!H", len(jsonheader_bytes))
        message = message_hdr + jsonheader_bytes + content_bytes
        self._send_buffer += message
        return
    
    def _jsonDecode(self, json_bytes, encoding):
        tiow = io.TextIOWrapper(io.BytesIO(json_bytes), encoding=encoding, newline="")
        obj = json.load(tiow)
        tiow.close()
        return obj
    
    def _jsonEncode(self, obj, encoding):
        return json.dumps(obj, ensure_ascii=False).encode(encoding)

    def _payloadDecode(self, data):
        self.payload = dill.loads(data)

    def _readData(self):
        content_length = self._jsonheader["content-length"]
        if len(self._receive_buffer) < content_length:
            self._receiveAllData(content_length)
        self._payloadDecode(self.payload)
    
    def _readFixedHeader(self):
        fixed_length_header = 2
        try:
            self._receive_buffer += self.socket.recv(fixed_length_header)
        except:
            return
        if len(self._receive_buffer) >= fixed_length_header:
            #Unpack returns a tuple, length of JSON header is element 0:
            self._jsonheader_len = struct.unpack("!H", self._receive_buffer[:fixed_length_header])[0]
            self._receive_buffer = self._receive_buffer[fixed_length_header:]
    
    def _readVariableHeader(self):
        variable_length_header = self._jsonheader_len
        self._receive_buffer += self.socket.recv(variable_length_header)

        if len(self._receive_buffer) >= variable_length_header:
            self._jsonheader = self._jsonDecode(self._receive_buffer[:variable_length_header], "utf-8")
            self._receive_buffer = self._receive_buffer[variable_length_header:]
            
            for reqhdr in ("byteorder","content-length","content-type","content-encoding", "destination", "active", "uuid_to", "uuid_from"):
                if reqhdr not in self._jsonheader:
                    raise ValueError(f'Missing required header "{reqhdr}".')
            
            if not self._jsonheader["active"]:
                logging.debug("Beginning shutdown in variable header.")
                self._shutdown()

            if not self._jsonheader["uuid_from"]:
                self._register()
        
    def _receiveAllData(self, n_bytes):
        while len(self.payload) < n_bytes:
            chunk = self.socket.recv(n_bytes - len(self.payload))
            if chunk == '':
                break
            self.payload += chunk
    
    def _register(self):
        uuid = self.uuid_func()
        self.registration_q.put((uuid,self.socket))
    
    def _resetBuffers(self):
        this_message = {}
        this_message["time"] = datetime.today().strftime('%Y-%m-%d-%H:%M:%S')
        this_message["response"] = self._send_buffer
        this_message["response_to"] = "echo client"
        this_message["request_from"] = self._jsonheader["uuid"]
        self.disk_q.put(this_message)
        
        self._jsonheader_len = None
        self._jsonheader = None

        self._receive_buffer = b""
        self._send_buffer = b""
        self.payload = b""
        
        self.active = False
        return 0
    
    def _shutdown(self):
        logging.debug("Shutdown received on server.")
        try:
            self.shutdown()
        except:
            pass
    
    def executeService(self):
        logging.debug(f"The server received a request from the client: {self.address}")
        response = self.service()
        self.sendMessage(self, message=response, active_flag=True)
    
    def listen(self):
        while True:
            try:
                self.receiveMessage()
            except OSError:
                time.sleep(0.5)
            except RuntimeError:
                logging.debug("Runtime err?")
                break
        return

    def receiveMessage(self):
        ##Attempt to receive a message and free control of process if miss:
        self._readFixedHeader()
        if not self._jsonheader_len:
            logging.debug("No json header length")
            time.sleep(0.1)
            return

        ##Received request- decode message and exit loop if shutdown was received:
        self._readVariableHeader()
        self.active = True
        logging.debug("msg received...")
        if self._exit:
            logging.debug("Error raised?")
            raise RuntimeError
        else:
            pass
    
        ##Collect request, execute a generic service, and reset to base state before looping:
        self._readData()
        logging.debug("msg received... 2")
        self.executeService()
        self._resetBuffers()
        return
    
    def sendMessage(self, message="The server is sending a ping request.", active_flag=True):
        ##Load the buffer
        self._buildMessage(content_type="text/pickle", content_encoding="utf-8", destination=self.address, active=active_flag, content_bytes=message)
        ##Still need to add destination to the response.
        self.io_q.put((self._send_buffer))
    
    def shutdown(self):
        '''Override method to exit the communication relay and close the connection.'''
        shutdown_bytes = struct.pack("!H", 0)
        try:
            self.sendMessage(message=shutdown_bytes, active_flag=False)
        except Exception as e:
            ##Should be that the socket is already closed.
            logging.debug(e)
        finally:
            self._exit = True

class ThreadedServer(object):
    """
    This is the main server that receives n connections simultaneously.
    Threads are managed through concurrent futures' Executors.
    Default functionality is an echo server but body functionality can be mutated by subclass or monkey patch.
    Architecture is:
        1) One thread to manage creation of other threads
        2) One thread to manage Open Connections
    When a client comes online, this action is logged by the connection queue:
        1) The client is logged and changed to the active queue
        2) The client socket, address, and host information is updated in the records.
    Every message gets passed through and logged by the (one of many) servers. Every server manages a live message feed and a dead message feed
        1) Live messages get checked in active connections, queued, and send FIFO.
        2) Dead messages fail the active connection check, and get deposited until receiving a connection from the designated client- which leads to re-queue.
    """
    
    def __init__(self, host="127.0.0.1", port=28282, io_q=None, disk_q=None, connection_q=None, registration_q=None, offline_q=None, workers=None, clients=None, online_clients=None):
        self.buildIncomingSocket(host=host, port=port)
        
        self.spool = queue.Queue()
        self.workers = workers or {}
        self.clients = clients or {}
        self.online_clients = online_clients or {}

        self.io_q = io_q or queue.Queue()
        self.disk_q = disk_q or queue.Queue()
        self.connection_q = connection_q or queue.Queue()
        self.offline_q = offline_q or queue.Queue()
        self.registration_q = registration_q or queue.Queue()
        
        if not os.path.isdir("./AppData/"):
            os.mkdir("./AppData/")
        if not os.path.isdir("./AppData/Database/"):
            os.mkdir("./AppData/Database/")
        if not os.path.isdir("./AppData/MessageStore/"):
            os.mkdir("./AppData/MessageStore/")
        if not os.path.isdir("./AppData/Payloads/"):
            os.mkdir("./AppData/Payloads/")
        
        self.executor = concurrent.futures.ThreadPoolExecutor()
        self.spool.put((self.executor.submit(self.listen), "This is main's listening thread."))
        self.startDaemon(func=self.connectionWorker, label="ConnectionWorker1")
        self.startDaemon(func=self.ioWorker, label="IOWorker1")
        self.startDaemon(func=self.ioWorker, label="IOWorker2")
            
    def buildIncomingSocket(self, host="127.0.0.1", port=12345):
        self.listening_host = host
        self.listening_port = port
        
        self.ingress_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.ingress_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.ingress_socket.bind((self.listening_host, self.listening_port))
        self.ingress_socket.listen(100)
        return
    
    def checkOfflineQueue(self, connection_info):
        logging.debug("Offline queue hit.")
        ##Iterate over offline queue members (offline queue members should be a sql db.)
            ##Check for those with ID matching incoming connection
            ##Re-Queue to destination with connection_info
        return
    
    def closeServer(self):
        self.executor.shutdown()
        self.saveToDB()
#         sys.exit(0)
        return

    def connectionWorker(self):
        '''Workers push messages from the IO queue to destinations.'''
        while True:
            try:
                logging.debug("Connection queue get...")
                uuid, status, socket, address = self.connection_q.get()
                logging.debug(uuid)
                self.clients[uuid] = {
                    "status": status,
                    "socket": socket,
                    "address": address,
                }
                logging.debug(self.clients)
                self.connection_q.task_done()
            except:
                time.sleep(0.5)
                continue
    
    def ioWorker(self):
        '''Workers push messages from the IO queue to destinations.'''
        while True:
            try:
                logging.debug("IO queue get...")
                client, message = self.io_q.get()
                logging.debug(client, message)
                if client in self.online_clients:
                    this_socket = self.lookupClient(client)
                    this_socket.sendall(message)
                else:
                    self.offline_q.put((client, message))
                logging.debug("io task executed.")
                self.io_q.task_done()
            except:
                time.sleep(0.5)
                continue
    
    def startDaemon(self, func, label):
        '''Workers push messages from the IO queue to destinations.'''       
        self.workers[label] = threading.Thread(target=func)
        self.workers[label].daemon = True
        self.workers[label].start()
        return

    def listen(self):
        while True:
            try:
                client, address = self.ingress_socket.accept()
                logging.debug(f"Connection from {address}")
                ##Will need to be erased as client connections grow larger.
                self.spool.put((self.executor.submit(self.newClient, client, address), address))
            except:
                time.sleep(0.1)
    
    def lookupClient(self, client_id, client_new_info):
        return self.clients[client_id]["socket"]
#         if client_id in self.online_clients:
#             self.online_clients[client_id]
#             return correct_socket
#         else:
#             self.clients[client_id] = client_new_info

    def newClient(self, client, address):
        '''The client keeps a messaging request-response open until one side sends a shutdown signal.'''
        try:
            logging.debug("Client thread starting.")
            msg = Messenger(socket=client, address=address, io_q=self.io_q, connection_q=self.connection_q, disk_q=self.disk_q, service=Service)
            msg.listen()
        except OSError as e:
            logging.debug(e)
        finally:
            logging.debug("Closing connection with client.")
    
    def registrationWorker(self):
        '''Workers push messages from the IO queue to destinations.'''
        while True:
            uuid, client = self.registration_q.get()
            logging.debug(uuid)
            self.clients[uuid] = client
            self.connection_q.put((uuid, True))
            self.registration_q.task_done()
    
    def saveToDB(self):
        logging.debug("Saving to database.")
        ###Timing tests and graphs for loading to pgsql- on exit or at n messages.
        ##Flush the disk_q to database
            ##Save io_q to db_q.
            ##Save offline_q to db_q.
            ##Save db_q to db.
        return




if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG, format='(%(threadName)-9s) %(message)s',)
    main = ThreadedServer()