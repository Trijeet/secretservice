#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import concurrent.futures
import csv
from datetime import datetime
import dill
import io
import json
import logging
import queue
import socket
import threading
import struct
import sys
import time

class Messenger:
    def __init__(self, socket=None, address=None):
        self.socket = socket
        self.address = address
        
        self._receive_buffer = b""
        self._send_buffer = b""
        self.payload = b""
        
        self._jsonheader_len = None
        self._jsonheader = None
        
        self.active = False
        self.log = []
    
    def _jsonEncode(self, obj, encoding):
        return json.dumps(obj, ensure_ascii=False).encode(encoding)

    def _jsonDecode(self, json_bytes, encoding):
        tiow = io.TextIOWrapper(io.BytesIO(json_bytes), encoding=encoding, newline="")
        obj = json.load(tiow)
        tiow.close()
        return obj
    
    def _payloadDecode(self, data):
        self.payload = dill.loads(data)
    
    def _buildMessage(self, content_type="text/pickle", content_encoding="utf-8", content_bytes="Request from client."):
        content_bytes = dill.dumps(content_bytes)
        jsonheader = {
            "byteorder": sys.byteorder,
            "content-type": content_type,
            "content-encoding": content_encoding,
            "content-length": len(content_bytes),
        }
        jsonheader_bytes = self._jsonEncode(jsonheader, "utf-8")
        message_hdr = struct.pack("!H", len(jsonheader_bytes))
        message = message_hdr + jsonheader_bytes + content_bytes
        self._send_buffer += message
        return
    
    def _shutdown(self):
        logging.debug("Shutdown received on server.")
        writer = csv.DictWriter(open(f"./Data/test_file{datetime.today().strftime('%Y-%m-%d-%H:%M:%S')}.csv", "w"), self.log[0].keys())
        writer.writeheader()
        writer.writerows(self.log)
        try:
            self.shutdown()
        except:
            pass
        raise RuntimeError
    
    def sendMessage(self, message="The server is sending a ping request."):
        ##Load the buffer
        self._buildMessage(content_type="text/pickle", content_encoding="utf-8", content_bytes=message)
        self.socket.sendall(self._send_buffer)
    
    def _readFixedHeader(self):
        fixed_length_header = 2
        self._receive_buffer += self.socket.recv(fixed_length_header)
        if len(self._receive_buffer) >= fixed_length_header:
            #Unpack returns a tuple, length of JSON header is element 0:
            self._jsonheader_len = struct.unpack("!H", self._receive_buffer[:fixed_length_header])[0]
            #The shutdown signal is a 0-length variable header:
            if self._jsonheader_len==0:
                self._shutdown()
            else:
                self._receive_buffer = self._receive_buffer[fixed_length_header:]
                return 1
    
    def _readVariableHeader(self):
        variable_length_header = self._jsonheader_len
        self._receive_buffer += self.socket.recv(variable_length_header)
        if len(self._receive_buffer) >= variable_length_header:
            self._jsonheader = self._jsonDecode(self._receive_buffer[:variable_length_header], "utf-8")
            self._receive_buffer = self._receive_buffer[variable_length_header:]
            
            for reqhdr in ("byteorder","content-length","content-type","content-encoding"):
                if reqhdr not in self._jsonheader:
                    raise ValueError(f'Missing required header "{reqhdr}".')
            
    def _readData(self):
        content_length = self._jsonheader["content-length"]
        if len(self._receive_buffer) < content_length:
            self._receiveAllData(content_length)
        self._payloadDecode(self.payload)
        
    def _receiveAllData(self, n_bytes):
        while len(self.payload) < n_bytes:
            chunk = self.socket.recv(n_bytes - len(self.payload))
            if chunk == '':
                break
            self.payload += chunk
    
    def receiveMessage(self):
        if self._readFixedHeader()==0:
            logging.debug(f"Received shutdown from {self.address}")
            return
        if not self._jsonheader_len:
            time.sleep(0.1)
            return
        self._readVariableHeader()
        self.active = True
        self._readData()
        this_data = self.payload
        self._resetBuffers()
        logging.debug(this_data)
        return this_data
    
    def shutdown(self):
        '''Override method to exit the communication relay and close the connection.'''
        shutdown_bytes = struct.pack("!H", 0)
        self.socket.sendall(shutdown_bytes)
    
    def _resetBuffers(self):
        this_message ={}
        this_message["time"] = datetime.today().strftime('%Y-%m-%d-%H:%M:%S')
        this_message["data"] = self.payload
        self.log.append(this_message)
        
        self._jsonheader_len = None
        self._jsonheader = None

        self._receive_buffer = b""
        self._send_buffer = b""
        self.payload = b""
        
        self.active = False
        return 0
    
    def listen(self):
        logging.debug("hit listener")
        while True:
            try:
                self.receiveMessage()
            except OSError:
                time.sleep(0.5)
        logging.debug("Out of while loop in listener.")
