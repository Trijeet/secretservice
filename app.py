#!/usr/bin/env python
# -*- encoding: utf-8 -*-

##Std Lib Imports:
import gc
import logging
import os
import string
import sys

##Outside imports:
import PySide2 as ps
from PySide2.QtWidgets import *
from PySide2.QtCore import QTimer, Slot, QDate, QFile, Qt
from PySide2.QtCharts import *
from PySide2.QtGui import *

##Internal imports:
from window import MainWindow

def mainwindowSetup(w: object) -> None:
    w.setWindowTitle("Connection Client")
    w.setWindowIcon(QIcon('./assets/icons/dice.svg'))
    size_policy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
    size_policy.setHorizontalStretch(0)
    size_policy.setVerticalStretch(0)
    w.setSizePolicy(size_policy)
    w.resize(300,650)
    # w.setMaximumSize(350,650)
    w.show()
    return

def main():
    logging.basicConfig(level=logging.DEBUG, format='(%(threadName)-9s) %(message)s',)
    app = QApplication(sys.argv)
    app.setStyle('Fusion')
    window = MainWindow(app_ref=app)
    mainwindowSetup(window)
    app.exec_()

if __name__ == '__main__':
    sys.exit(main()) 