#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import concurrent.futures
import dill
import json
import logging
import queue
import socket
import struct
import sys
import threading
import time

from messenger import Messenger

class Client:
    
    def __init__(self, host="127.0.0.1", port=12345,messenger=None,data_log=None,io_q=None):
        self.host = host
        self.port = port
        self.data_log = data_log or queue.Queue()
        
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((self.host, self.port))
        
        self.messenger = messenger or Messenger(socket=self.socket)
        
        self.io_q = io_q or queue.Queue()
        
        self.executor = concurrent.futures.ThreadPoolExecutor()
        self.io_q.put((self.executor.submit(self.newClient), "This is the client's main listening thread."))
    
    def sendMsg(self, message = "this is my long string story it does lots of string things"):
        if not self.socket:
            print("There's no open connection")
            return
        self.messenger.sendMessage(message=message)
    
    def getMsg(self):
        if not self.socket:
            print("There's no open connection")
            return
        self.data_log.put(self.messenger.receiveMessage())
        logging.debug(self.data_log)

    def newChat(self):
        self.io_q.put((self.executor.submit(self.newClient), "A new client chat has been opened."))

    def newClient(self):
        try:
            logging.debug("Client listener started.")
            Messenger(socket=self.socket).listen()
            logging.debug("Hit new client")
        except OSError as e:
            logging.debug(e)
        finally:
            logging.debug("Closing time.")
            self.socket.close()
    
    def sendShutdown(self):
        '''Override method to exit the communication relay and close the connection.'''
        shutdown_bytes = struct.pack("!H", 0)
        self.socket.sendall(shutdown_bytes)
    
    def close(self):
        '''Send the 0 length fixed header to shutdown the connection.'''
        print("closing connection to", self.host)

        try:
            try:
                self.sendShutdown()
            except:
                pass
            self.messenger.shutdown()
            self.socket.close()
        except OSError as e:
            print(f"error: socket.close() exception for {self.host}: {repr(e)}")
        finally:
            self.socket = None