#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import concurrent.futures
import csv
from datetime import datetime
import dill
import io
import json
import logging
import os
import queue
import socket
import threading
import struct
import sys
import time

from service import Service

class ThreadedServer(object):
    """
    This is the main server that receives n connections simultaneously.
    Threads are managed through concurrent futures' Executors.
    Default functionality is an echo server but body functionality can be mutated by subclass or monkey patch.
    Architecture is:
        1) One thread to manage creation of other threads
        2) One thread to manage Open Connections
    """
    
    def __init__(self, host="127.0.0.1", port=12345, connection_q=None, io_q=None):
        self.host = host
        self.port = port
        
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((self.host, self.port))
        
        self.connection_q = connection_q or queue.Queue()
        self.io_q = io_q or queue.Queue()
        
        if not os.path.isdir("./Data/"):
            os.mkdir("./Data/")
        
        self.executor = concurrent.futures.ThreadPoolExecutor()
        
        self.connection_q.put((self.executor.submit(self.listen), "This is main's listening thread."))
    
    def closeServer(self):
        self.executor.shutdown()
        return 0

    def listen(self):
        self.sock.listen(100)
        while True:
            client, address = self.sock.accept()
            print('Connected by', address)
            self.connection_q.put((self.executor.submit(self.newClient, client, address), address))

    def newClient(self, client, address):
        '''The client keeps a messaging request-response open until one side sends a shutdown signal.'''
        try:
            logging.debug("client thread started.")
            Service(socket=client, address=address).listen()
        except OSError as e:
            logging.debug(e)
        finally:
            logging.debug("closing time.")
            client.close()

if __name__ == '__main__':
	logging.basicConfig(level=logging.DEBUG, format='(%(threadName)-9s) %(message)s',)
	main = ThreadedServer()