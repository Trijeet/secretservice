#!/usr/bin/env python
# -*- encoding: utf-8 -*-

##Std Lib Imports:
import gc
import os
import string
import sys

##Outside imports:
import PySide2 as ps
from PySide2.QtWidgets import *
from PySide2.QtCore import QTimer, Slot, QDate, QFile, Qt
from PySide2.QtCharts import *
from PySide2.QtGui import *

##Internal Imports
from connection import Connection
from client import Client
from server import ThreadedServer

class MainWindow(QMainWindow):
    
    def __init__(self, app_ref):
        QMainWindow.__init__(self)
        self.app_ref = app_ref
        self.client = Client()
        self.server = ThreadedServer()
        self.setWindowTitle("Entry Frame")

        ##Bring in the style sheet:
        stylesheet_path = os.path.join(os.path.dirname(__file__), 'style.css')

        with open(stylesheet_path, 'r') as f:
            self.setStyleSheet(f.read())
        self.buildPalettes()
        
        ##Create main window pieces:
        self.buildMenu()

        ##Content
        stackedWidget = QStackedWidget()
        stackedWidget.addWidget(Connection(obj_ref=self))
        self.setCentralWidget(stackedWidget)


    def buildMenu(self) -> None:
        #Menu options:
        self.menu = self.menuBar()
        self.machine_menu = self.menu.addMenu("&Connections")

        ##New:
        new_action = QAction("&New Connection", self)
        new_action.setStatusTip("Create a new finite state machine.")
        new_action.setIcon(QPixmap('./assets/icons/material/note_add-24px.svg'))
        new_action.setShortcut("Ctrl+N")
        new_action.triggered.connect(self.newMachine)
        self.machine_menu.addAction(new_action) 
        
        # ##Open:
        open_action = QAction("&Open Connection", self)
        open_action.setStatusTip("Open an existing finite state machine.")
        open_action.setIcon(QPixmap('./assets/icons/material/folder_open-24px.svg'))
        open_action.setShortcut("Ctrl+O")
        open_action.triggered.connect(self.openMachine)
        self.machine_menu.addAction(open_action)

        ##Save:
        save_action = QAction("&Save Log", self)
        save_action.setStatusTip("Save the current finite state machine.")
        save_action.setIcon(QPixmap('./assets/icons/material/save-24px.svg'))
        save_action.setShortcut("Ctrl+S")
        save_action.triggered.connect(self.saveMachine)
        self.machine_menu.addAction(save_action) 

        self.machine_menu.addSeparator()

        ##Print Diagram:
        exit_action = QAction("&Print Log", self)
        exit_action.setStatusTip("Print the diagram for the current machine.")
        exit_action.setIcon(QPixmap('./assets/icons/material/print-24px.svg'))
        exit_action.setShortcut("Ctrl+P")
        exit_action.triggered.connect(self.printDiagram)
        self.machine_menu.addAction(exit_action)

        self.machine_menu.addSeparator()

        ##Exit:
        exit_action = QAction("&Exit", self)
        exit_action.setStatusTip("Quit the application.")
        exit_action.setIcon(QPixmap('./assets/icons/material/power_settings_new-24px.svg'))
        exit_action.setShortcut("Ctrl+Q")
        exit_action.triggered.connect(self.exitApp)
        self.machine_menu.addAction(exit_action)

        #STATE MENU

        self.state_menu = self.menu.addMenu("&States")

        ##New:
        new_state_action = QAction("&New State", self)
        new_state_action.setShortcut("Ctrl+T")
        self.state_menu.addAction(new_state_action)

        #VIEW MENU

        self.view_menu = self.menu.addMenu("&View")

        self.dark_mode = False
        toggle_dark_mode = QAction("&Dark Mode", self)
        toggle_dark_mode.triggered.connect(self.toggleDarkTheme)
        self.view_menu.addAction(toggle_dark_mode)


        #ABOUT MENU
        self.help_menu = self.menuBar().addMenu("&About")

        ##View About
        view_about_action = QAction("&About", self)
        view_about_action.triggered.connect(self.about)
        self.help_menu.addAction(view_about_action)

        return

    def about(self):
        QMessageBox.about(self, "About Secret Service",
                "Project to demonstrate end-to-end communication between two clients and a host with application level protocols."
                "Serves as a front-end for the code hosted in <a href='https://gitlab.com/Trijeet/secretservice'> this repository </a> "
                "All work is &copy; 2020 Trijeet Sethi")

    @Slot()
    def exitApp(self, checked):
        '''Wrapper to Qt's quit application.'''
        QApplication.quit()
        return

    @Slot()
    def newMachine(self, checked):
        '''Creates a new FSM project.'''
        text, ok = QInputDialog().getText(self, "Create a new FSM",
                                     "Enter a name for this machine:", QLineEdit.Normal)
        print(text,ok)
        return

    @Slot()
    def openMachine(self, checked):
        '''Reads in an existing FSM project.'''
        print("Read machine.")
        return

    @Slot()
    def printDiagram(self, checked):
        '''Print the diagram for an existing FSM project.'''
        print("Printing machine.")
        return      
   
    @Slot()
    def saveMachine(self, checked):
        '''Saves an existing FSM project.'''
        print("Saves a machine.")
        # filename, _ = QFileDialog.getSaveFileName(self,
        #         "Choose a file name", '.', "HTML (*.html *.htm);;Text Files (*.txt);;")
        # if not filename:
        #     return

        # file = QFile(filename)
        # if not file.open(QFile.WriteOnly | QFile.Text):
        #     QMessageBox.warning(self, "Dock Widgets",
        #             "Cannot write file %s:\n%s." % (filename, file.errorString()))
        #     return

        # ##Text stream a save to the file:
        # out = QTextStream(file)
        # QApplication.setOverrideCursor(Qt.WaitCursor)
        # out << self.textEdit.toHtml()
        # QApplication.restoreOverrideCursor()
        return

    @Slot()
    def buildPalettes(self):
        self.default_palette = QPalette()
        self.dark_palette = QPalette()
        self.dark_palette.setColor(QPalette.Window, QColor(53, 53, 53))
        self.dark_palette.setColor(QPalette.WindowText, Qt.white)
        self.dark_palette.setColor(QPalette.Base, QColor(25, 25, 25))
        self.dark_palette.setColor(QPalette.AlternateBase, QColor(53, 53, 53))
        self.dark_palette.setColor(QPalette.ToolTipBase, Qt.white)
        self.dark_palette.setColor(QPalette.ToolTipText, Qt.white)
        self.dark_palette.setColor(QPalette.Text, Qt.white)
        self.dark_palette.setColor(QPalette.Button, QColor(53, 53, 53))
        self.dark_palette.setColor(QPalette.ButtonText, Qt.white)
        self.dark_palette.setColor(QPalette.BrightText, Qt.red)
        self.dark_palette.setColor(QPalette.Link, QColor(42, 130, 218))
        self.dark_palette.setColor(QPalette.Highlight, QColor(42, 130, 218))
        self.dark_palette.setColor(QPalette.HighlightedText, Qt.black)
        return

    @Slot()
    def toggleDarkTheme(self):
        if not self.dark_mode:
            self.app_ref.setPalette(self.dark_palette)
        else:
            self.app_ref.setPalette(self.default_palette)
