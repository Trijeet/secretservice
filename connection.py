#!/usr/bin/env python
# -*- encoding: utf-8 -*-

##Std Lib Imports:
import gc
import os
import string
import sys

##Outside imports:
import PySide2 as ps
from PySide2.QtWidgets import *
from PySide2.QtCore import QTimer, Slot, QDate, QFile, Qt, QSize, QEvent, QModelIndex
from PySide2.QtCharts import *
from PySide2.QtGui import *

##Internal Imports
from client import Client

class Connection(QWidget):
    '''
    Widget for one communication channel:
    1) Exposes interface to send messages
    2) Logs the chat history
    3) Allows for attachments
    '''

    def __init__(self, obj_ref: object):
        QWidget.__init__(self)
        self.obj_ref = obj_ref
        self.client = self.obj_ref.client

        ##Main pieces:
        self.buildDisplay()
        self.buildLog()
        self.buildMessenger()
        self.buildAttachments()
        self.buildButtonMenu1()
        self.buildButtonMenu2()

        ##Compile into the four-part client:
        self.fullgrid = QVBoxLayout()
        self.fullgrid.addLayout(self.display_box)
        self.fullgrid.addLayout(self.log_box)
        self.fullgrid.addLayout(self.chat_box)
        self.fullgrid.addLayout(self.attach_box)
        # self.fullgrid.addLayout(self.side_box1)
        # self.fullgrid.addLayout(self.side_box2)

        # self.fullgrid.QGridLayout()
        # self.fullgrid.setColumnStretch(1,0)
        # self.fullgrid.setColumnStretch(1,1)
        # self.fullgrid.setColumnStretch(1,2)
        # self.fullgrid.setColumnStretch(1,3)
        # self.fullgrid.setColumnMinimumWidth(2,100)
        # self.fullgrid.addLayout(self.display_box,0,1)
        # self.fullgrid.addLayout(self.log_box,1,1)
        # self.fullgrid.addLayout(self.chat_box,2,1)
        # self.fullgrid.addLayout(self.attach_box,3,1)
        # self.fullgrid.addLayout(self.side_box1,0,0)
        # self.fullgrid.addLayout(self.side_box2,3,0)

        self.setLayout(self.fullgrid)

        ##Populate with some fake data:
        for i in range(1,31):
            self.addMessage(f"Message number {i}")

        self.text_editor.setFocus()
        self.client.newChat()

    def buildDisplay(self):
        ##Define containers:
        self.display_box = QHBoxLayout()

        ##Components:
        self.exit_connection = QPushButton("")
        self.exit_connection.setIcon(QPixmap('./assets/icons/material/arrow_back-24px.svg'))
        self.exit_connection.setFixedSize(QSize(50, 30))
        self.connected_name = QLabel("Connected to Server")
        self.connection_menu = QPushButton("")
        self.connection_menu.setIcon(QPixmap('./assets/icons/material/menu-24px.svg'))
        self.connection_menu.setFixedSize(QSize(50, 30))

        ###Log history line:
        self.display_box.addWidget(self.exit_connection)
        self.display_box.addWidget(self.connected_name)
        self.display_box.addWidget(self.connection_menu)
        return

    def buildLog(self):
        ##Define containers:
        self.log_box = QVBoxLayout()
        self.log_box.setContentsMargins(55, 10, 55, 10)

        ##Components:
        self.message_log = QListWidget()
        self.message_log.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        # self.message_log.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        # self.scroll.setWidgetResizable(True)

        ###Log history line:
        self.log_box.addWidget(self.message_log)
        return

    def buildMessenger(self):
        ##Define containers:
        self.chat_box = QHBoxLayout()
        self.chat_box.setSizeConstraint(QLayout.SetFixedSize)
        self.chat_box.setSpacing(20)
        self.chat_box.setContentsMargins(55, 10, 55, 10)

        ##Components:
        self.text_editor = QTextEdit()
        self.text_editor.installEventFilter(self)
        self.text_editor.setFixedHeight(75)
        self.flush_text = QPushButton("Send")
        self.flush_text.setFixedSize(QSize(75, 45))
        self.flush_text.setDefault(False)
        self.flush_text.setAutoDefault(True)
        self.flush_text.setProperty("formSubmitButton", True)
        self.flush_text.setShortcut("Enter")

        ##Signals:
        self.flush_text.clicked.connect(self.sendMessage)

        ##Layout:
        self.chat_box.addWidget(self.text_editor)
        self.chat_box.addWidget(self.flush_text)
        return

    def buildAttachments(self):
        ##Define containers:
        self.attach_box = QHBoxLayout()
        self.attach_box.setSpacing(5)
        self.attach_box.setContentsMargins(55, 10, 400, 10)

        ##Components:
        self.add_file = QPushButton("")
        self.add_file.setIcon(QPixmap('./assets/icons/material/attach_email-24px.svg'))
        self.add_photo = QPushButton("")
        self.add_photo.setIcon(QPixmap('./assets/icons/material/add_photo_alternate-24px.svg'))
        self.add_sound = QPushButton("")
        self.add_sound.setIcon(QPixmap('./assets/icons/material/music_video-24px.svg'))
        self.add_data = QPushButton("")
        self.add_data.setIcon(QPixmap('./assets/icons/material/file_copy-24px.svg'))

        self.add_file.setFixedSize(QSize(50, 30))
        self.add_photo.setFixedSize(QSize(50, 30))
        self.add_sound.setFixedSize(QSize(50, 30))
        self.add_data.setFixedSize(QSize(50, 30))

        ###Log history line:
        self.attach_box.addWidget(self.add_file)
        self.attach_box.addWidget(self.add_photo)
        self.attach_box.addWidget(self.add_sound)
        self.attach_box.addWidget(self.add_data)
        return

    def buildButtonMenu1(self):
        ##Define containers:
        self.side_box1 = QVBoxLayout()

        ##Components:
        self.a1 = QPushButton("")
        self.a1.setIcon(QPixmap('./assets/icons/material/arrow_back-24px.svg'))
        self.b1 = QPushButton("Connected to Server")
        self.c1 = QPushButton("")
        self.c1.setIcon(QPixmap('./assets/icons/material/menu-24px.svg'))

        ###Log history line:
        self.side_box1.addWidget(self.a1)
        self.side_box1.addWidget(self.b1)
        self.side_box1.addWidget(self.c1)
        return

    def buildButtonMenu2(self):
        ##Define containers:
        self.side_box2 = QVBoxLayout()

        ##Components:
        self.a2 = QPushButton("")
        self.a2.setIcon(QPixmap('./assets/icons/material/arrow_back-24px.svg'))
        self.b2 = QPushButton("Connected to Server")
        self.c2 = QPushButton("")
        self.c2.setIcon(QPixmap('./assets/icons/material/menu-24px.svg'))

        ###Log history line:
        self.side_box2.addWidget(self.a2)
        self.side_box2.addWidget(self.b2)
        self.side_box2.addWidget(self.c2)
        return

    #Begin functions:
    def addMessage(self, message="Debug posted this message."):
        '''Add message to the history log.'''
        self.message_log.addItem(message)
        item = self.message_log.item(int(self.message_log.count())-1)
        self.message_log.scrollToItem(item, QAbstractItemView.PositionAtTop)
        self.message_log.setCurrentItem(item)
        return

    def updateLabel(self, label, text):
        label.setText(text)
        self.repaint()
        return

    def sendMessage(self):
        payload = self.text_editor.toMarkdown()
        self.text_editor.clear()
        try:
            if payload=="":
                return
            else:
                payload = payload.strip()
                self.addMessage(message=payload)
                self.client.sendMsg(message=payload)
        except Exception as e:
            payload = payload.encode("utf-8")
        return

    def selectMessage(self, item):
        print(item, str(item.text()))
        return

    def eventFilter(self, obj, event):
        if event.type() == QEvent.KeyPress and obj is self.text_editor:
            if event.key() == Qt.Key_Return and self.text_editor.hasFocus():
                self.sendMessage()
                self.text_editor.moveCursor(QTextCursor.Start)
                self.text_editor.clear()
                self.repaint()
                self.text_editor.textCursor()
                self.text_editor.moveCursor(QTextCursor.Start)
                self.repaint()
        return super().eventFilter(obj, event)