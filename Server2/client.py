#!/usr/bin/env python
# -*- encoding: utf-8 -*-

##Imports
import concurrent.futures
import csv
from datetime import datetime
import dill
import io
import json
import logging
import os
import queue
import socket
import threading
import struct
import sys
import time

class Messenger:
    def __init__(self, uuid=None, io_queue=None, socket=None, address=None, client_ref=None):
        self.io_q = io_queue
        self.socket = socket
        self.address = address
        self.client_ref = client_ref
       
        self._receive_buffer = b""
        self._send_buffer = b""
        self.payload = b""
        
        self._jsonheader_len = None
        self._jsonheader = None
        
        self.active = False
        self.log = []
        
        if not uuid:
            logging.debug("asking for uuid")
            self._register()
            self.client_ref.uuid = self.uuid
            logging.debug("uuid set.")
        else:
            self.uuid=uuid
    
    def _buildMessage(self, content_type="text/pickle", content_encoding="utf-8", destination="Echo client", active=True, uuid_to=False, uuid_from=False, content_bytes="Request from client."):
        content_bytes = dill.dumps(content_bytes)
        jsonheader = {
            "byteorder": sys.byteorder,
            "content-type": content_type,
            "content-encoding": content_encoding,
            "content-length": len(content_bytes),
            "destination": destination,
            "active": active,
            "uuid_to": uuid_to,
            "uuid_from": uuid_from,
        }
        jsonheader_bytes = self._jsonEncode(jsonheader, "utf-8")
        message_hdr = struct.pack("!H", len(jsonheader_bytes))
        message = message_hdr + jsonheader_bytes + content_bytes
        self._send_buffer += message
        return

    def _getUUID(self):
        return self.uuid
    
    def _jsonEncode(self, obj, encoding):
        return json.dumps(obj, ensure_ascii=False).encode(encoding)

    def _jsonDecode(self, json_bytes, encoding):
        tiow = io.TextIOWrapper(io.BytesIO(json_bytes), encoding=encoding, newline="")
        obj = json.load(tiow)
        tiow.close()
        return obj
    
    def _listen(self):
        while True:
            try:
                ##Pull a message from the read queue
                self.receiveMessage()
            except OSError:
                time.sleep(0.5)
            time.sleep(0.25)
    
    def _payloadDecode(self, data):
        self.payload = dill.loads(data)

    def _readData(self):
        content_length = self._jsonheader["content-length"]
        if len(self._receive_buffer) < content_length:
            self._receiveAllData(content_length)
        self._payloadDecode(self.payload)
        
    def _readFixedHeader(self):
        fixed_length_header = 2
        self._receive_buffer += self.socket.recv(fixed_length_header)
        if len(self._receive_buffer) >= fixed_length_header:
            #Unpack returns a tuple, length of JSON header is element 0:
            self._jsonheader_len = struct.unpack("!H", self._receive_buffer[:fixed_length_header])[0]
            #The shutdown signal is a 0-length variable header:
            if self._jsonheader_len==0:
                self._shutdown()
            else:
                self._receive_buffer = self._receive_buffer[fixed_length_header:]
                return 1
        
    def _readVariableHeader(self):
        variable_length_header = self._jsonheader_len
        self._receive_buffer += self.socket.recv(variable_length_header)
        if len(self._receive_buffer) >= variable_length_header:
            self._jsonheader = self._jsonDecode(self._receive_buffer[:variable_length_header], "utf-8")
            self._receive_buffer = self._receive_buffer[variable_length_header:]
            
            for reqhdr in ("byteorder","content-length","content-type","content-encoding", "destination", "active", "uuid_to", "uuid_from"):
                if reqhdr not in self._jsonheader:
                    raise ValueError(f'Missing required header "{reqhdr}".')
            
            if self._jsonheader["uuid_from"]=="server":
                self._register()
    
    def _receiveAllData(self, n_bytes):
        while len(self.payload) < n_bytes:
            chunk = self.socket.recv(n_bytes - len(self.payload))
            if chunk == '':
                break
            self.payload += chunk
    
    def _receiveMessage(self):
        if self._readFixedHeader()==0:
            logging.debug(f"Received shutdown from {self.address}")
            return
        if not self._jsonheader_len:
            time.sleep(0.1)
            return
        self._readVariableHeader()
        self.active = True
        self._readData()
        this_data = self.payload
        self._resetBuffers()
        logging.debug(this_data)
        return this_data

    def _register(self):
        ##read the message and set the uuid:
        self._buildMessage(content_type="text/pickle", content_encoding="utf-8", uuid_to=False, uuid_from=False, content_bytes="")
        self.socket.sendall(self._send_buffer)
        logging.debug("Sent message")
        
        uuid = None
        while not uuid:
            self._readFixedHeader()
            self._readVariableHeader()
            self._readData()
            uuid = self.payload
            logging.debug(uuid)
        self.uuid = uuid
    
    def _resetBuffers(self):
        this_message ={}
        this_message["time"] = datetime.today().strftime('%Y-%m-%d-%H:%M:%S')
        this_message["data"] = self.payload
        self.log.append(this_message)
        
        self._jsonheader_len = None
        self._jsonheader = None

        self._receive_buffer = b""
        self._send_buffer = b""
        self.payload = b""
        
        self.active = False
        return 0
    
    def _sendMessage(self, message="The server is sending a ping request."):
        ##Load the buffer
        self._buildMessage(content_type="text/pickle", content_encoding="utf-8", uuid_from=self.uuid, uuid_to="server", content_bytes=message)
        self.socket.sendall(self._send_buffer)
    
    def _sendShutdown(self):
        '''Override method to exit the communication relay and close the connection.'''
        shutdown_bytes = struct.pack("!H", 0)
        self.socket.sendall(shutdown_bytes)
    
    def _shutdown(self):
        writer = csv.DictWriter(open(f"./Data/test_file{datetime.today().strftime('%Y-%m-%d-%H:%M:%S')}.csv", "w"), self.log[0].keys())
        writer.writeheader()
        writer.writerows(self.log)
        try:
            self.shutdown()
        except:
            pass
        raise RuntimeError

class Client:
    '''
    Client class, provides interface for the GUI and manages threads and connections.
    The client provides a connection to the server, which holds all messages while the client is offline.
    Online, the client sends and receives messages to the server- info embedded into the Messages.
    '''
    def __init__(self, host="127.0.0.1", port=28282,messenger=None,client_data="./client_data.json",ingress_q=None, egress_q=None):
        self.host = host
        self.port = port
        self.spool = queue.Queue()
        
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((self.host, self.port))
        
        self.ingress_q = ingress_q or queue.Queue()
        self.egress_q = egress_q or queue.Queue()
        
        self.executor = concurrent.futures.ThreadPoolExecutor() 
        
        client_data_flag = os.path.isfile(client_data)
        
        if client_data_flag:
            client_data = {}
            self.message_log = queue.Queue()
            self.data_log = "./Data/"
            self.peer_log = {}
            self.preference_log = {}
            self.uuid = self._register()
        else:
            with open(client_data, 'r') as f:
                client_data_read = json.load(f)
            self.message_log = queue.Queue(client_data_read["messages"])
            self.data_log = client_data_read["data_path"]
            self.peer_log = client_data_read["peers"]
            self.preference_log = client_data_read["preferences"]
            self.uuid = client_data_read["uuid"]

#         self.messenger = messenger or Messenger() #need to preload with self arguments if we keep
        self.spool.put(self.executor.submit(self.newReader))
        self.spool.put(self.executor.submit(self.newWriter))
    
    def _register(self):
        x = Messenger(uuid=None, socket=self.socket, address=self.host, client_ref=self)
        return x.uuid
    
    def close(self):
        '''Send the 0 length fixed header to shutdown the connection.'''
        logging.debug(f"closing connection to {self.host}")
        try:
            try:
                self.sendShutdown()
            except:
                pass
            self.messenger.shutdown()
            self.socket.close()
        except OSError as e:
            print(f"error: socket.close() exception for {self.host}: {repr(e)}")
        finally:
            self.socket = None
    
    def getMsg(self):
        if not self.socket:
            logging.debug("There's no open connection")
            return
        self.data_log.put(self.messenger.receiveMessage())
        logging.debug(self.data_log)
    
    def newChat(self, peers=[None]):
        for i in peers:
            self.spool["main_listener"] = threading.Thread(target=self.newClient).start()
            self.io_q.put((self.executor.submit(self.newClient), "A new client chat has been opened."))

    def newReader(self):
        try:
            logging.debug("Client listener started.")
            Messenger(uuid=self.uuid, io_queue=self.ingress_q, socket=self.socket)._listen()
        except OSError as e:
            logging.debug(e)
        finally:
            logging.debug("Closing reader.")
    
    def newWriter(self):
        try:
            logging.debug("Client writer started.")
            Messenger(uuid=self.uuid, io_queue=self.egress_q, socket=self.socket)._listen()
        except OSError as e:
            logging.debug(e)
        finally:
            logging.debug("Closing writer.")
    
    def peerLookup(self, identifier):
        if identifier in self.peer_log:
            peer_host = self.peer_log[identifier]["host"]
            peer_port = self.peer_log[identifier]["port"]
            return identifier, peer_host, peer_port
        else:
            self.peer_log[identifier] = {}
    
    def sendMsg(self, message = "this is my long string story it does lots of string things"):
        if not self.socket:
            print("There's no open connection")
            return
        self.messenger.sendMessage(message=message)
    
    def sendShutdown(self):
        '''Override method to exit the communication relay and close the connection.'''
        shutdown_bytes = struct.pack("!H", 0)
        self.socket.sendall(shutdown_bytes)

if __name__ == '__main__':
	logging.basicConfig(level=logging.DEBUG, format='(%(threadName)-9s) %(message)s',)
	client2 = threading.Thread(target=Client)
	client2.start()